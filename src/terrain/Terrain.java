/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package terrain;

import connector.CRUD_opperations;
import connector.History;
import game_essentials.Game;
import game_essentials.Round;
import game_essentials.ThreadTimer;
import graphics.LoggerFrame;
import graphics.WellcomeFrame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 *
 * @author onelove
 */
public abstract class Terrain extends JFrame {

    protected Deck deck;
    protected GridLayout experimentLayout;
    protected BufferedImage backround;
    protected static ThreadTimer timer;
    public final int ttl;
    protected JButton restart_game;
    protected JButton new_game;
    protected CRUD_opperations db;

    public Terrain(int ttl, int width, int height) throws IOException {
        super("Game Terrain");
        timer = new ThreadTimer(this);
        deck = new Deck();
        backround = ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/bk.jpg"));
        this.ttl = ttl;

        restart_game = new JButton("Restart");
        restart_game.setSize(100, 50);
        restart_game.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                timer.interrupt();
                try {
                    WellcomeFrame.getGameObj().rest_game();
                } catch (IOException ex) {
                    Logger.getLogger(Terrain.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        new_game = new JButton("New Game");
        new_game.setSize(100, 50);
        new_game.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                timer.interrupt();
                WellcomeFrame.getGameObj().new_game();
            }
        });
        this.setDefaultCloseOperation(JFrame.NORMAL);
        this.setVisible(false);
        this.setLayout(experimentLayout);
        this.setResizable(false);
        this.setSize(width, height);
    }

    protected void start_timer() {
        timer.start();
    }

    public abstract void intiializeTerrain(Round player1, Round player2);

    public void setDeck(Deck deck) {
        this.deck = deck;
    }

    public void setExperimentLayout(GridLayout experimentLayout) {
        this.experimentLayout = experimentLayout;
    }

    public void refresh_Gui() {
        SwingUtilities.updateComponentTreeUI(this);

    }

    public void terminateGame(boolean hey) {
        LoggerFrame.append_text_to_logger("Game is terminated because of the time limit reached.");
        LoggerFrame.append_text_to_logger(Game.culculateWinner());
//        this.dispose();

        //Custom button text
        Object[] options = {"Save and Continue.",
            "Continue without Saving."};
        int reply = JOptionPane.showOptionDialog(this,
                "The game has come to an end because of the time limit reached, the result is : " + Game.culculateWinner(),
                "Game Result",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null, //do not use a custom Icon
                options, //the titles of buttons
                options[0]); //default button title
        if (reply == 0) {
            LoggerFrame.append_text_to_logger("Saving game result...");
            History h = Game.historyResult();
            //sroring in sql create an sql db and uncoment it to work :)
//            try {
//                db.insertData(h.getPlayer_1(), h.getPlayer_2(), h.getPoints_1(), h.getPoints_2());
//            } catch (SQLException ex) {
//                Logger.getLogger(Terrain.class.getName()).log(Level.SEVERE, null, ex);
//            }
            WellcomeFrame.getGameObj().new_game();
            System.out.println("Save");
        } else if (reply == 1) {
            LoggerFrame.append_text_to_logger("Return to main screen without saving....");
            WellcomeFrame.getGameObj().new_game();
        }
    }

    public void terminateGame() {
        LoggerFrame.append_text_to_logger("Game is terminated because all pairs have been found");
        LoggerFrame.append_text_to_logger(Game.culculateWinner());
//        this.dispose();
        //Custom button text
        Object[] options = {"Save and Continue.",
            "Continue without Saving."};
        int reply = JOptionPane.showOptionDialog(this,
                "The game has come to an end the result is : " + Game.culculateWinner(),
                "Game Result",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null, //do not use a custom Icon
                options, //the titles of buttons
                options[0]); //default button title
        if (reply == 0) {
            LoggerFrame.append_text_to_logger("Saving game result...");
            History h = Game.historyResult();
            //sroring in sql create an sql db and uncoment it to work :)
//            try {
//                db.insertData(h.getPlayer_1(), h.getPlayer_2(), h.getPoints_1(), h.getPoints_2());
//            } catch (SQLException ex) {
//                Logger.getLogger(Terrain.class.getName()).log(Level.SEVERE, null, ex);
//            }
            WellcomeFrame.getGameObj().new_game();

            System.out.println("Save");
        } else if (reply == 1) {
            LoggerFrame.append_text_to_logger("Return to main screen without saving....");
            //close thread by interapting
            timer.interrupt();
            //initialize starting screen
            WellcomeFrame.getGameObj().new_game();
        }
    }

    protected void show_information_msg(String msg) {
        JOptionPane.showMessageDialog(this, msg,
                "information", JOptionPane.INFORMATION_MESSAGE);
    }

    protected void setTerrainVisible(boolean temp) {
        this.setVisible(temp);
    }

}
