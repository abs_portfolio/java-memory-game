/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package terrain;

import card.Card;
import game_essentials.Game;
import game_essentials.Round;
import game_essentials.ThreadTimer;
import graphics.LoggerFrame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingUtilities;

/**
 *
 * @author onelove
 */
public class Terrain_Easy extends Terrain {

    private final static int bonus = 10; // bonus for a succesfull pair 
    private final static int row = 3; //rows of table
    private final static int col = 4; // cols of table
    private JButton[] buttonGrid; // table of buttons itself
    private int players_turn = 0; // define players turn
    private List<Integer> deleted_items; // pairs have been found
    private static Random rand = new Random();
    private static ThreadTimer timer;

    //overwite default constractor to generate the basic attributes
    public Terrain_Easy() throws IOException {
        super(4 * 60 * 1000, 900 , 600);
        this.setExperimentLayout(new GridLayout(row +1, col));
        this.setLayout(experimentLayout);
        deleted_items = new ArrayList();
    }

    private Card getCardByName(String name) {
        for (int i = 0; i < deck.getDeckSize(); i++) {
            if (deck.get_card_at_pos(i).get().equalsIgnoreCase(name)) {
                return deck.get_card_at_pos(i);
            }
        }
        return null;
    }

    @Override
    public void refresh_Gui() {
        SwingUtilities.updateComponentTreeUI(this);

    }

    @Override
    public void intiializeTerrain(Round player1, Round player2) {
        //initialize timer to 4 minutes
        start_timer();

        //initialize deck
        this.deck.initialize_deck_easy();
        this.deck.shuffleArray();
        this.deck.printDeck();
        this.buttonGrid = new JButton[row * col];
        Game.set_pairs((row * col) / 2);

        //create action listener
        ActionListener listener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() instanceof JButton) {
                    String text = ((JButton) e.getSource()).getText();
                    Card card = getCardByName(text);

                    ((JButton) e.getSource()).setIcon(new ImageIcon(card.getCard_image()));
                    refresh_Gui();

                    switch (players_turn) {
                        case 0: {
                            LoggerFrame.append_text_to_logger("***********************************************");
                            System.out.println("***********************************************");
                            LoggerFrame.append_text_to_logger("Plyaers " + player1.getPlayer().getName() + " Turn");
                            LoggerFrame.append_text_to_logger("Round " + player1.getMoves() + " ");

                            System.out.println("Plyaers " + player1.getPlayer().getName() + " Turn");
                            System.out.println("Round " + player1.getMoves() + " ");
                            System.out.println("");
                            System.out.println(text);

                            int play = player1.playRound(card);
                            if (play == 2) {
                                show_information_msg("You have found a pair GJ!!!!σ");
                                System.out.println("play on 2");
                                LoggerFrame.append_text_to_logger(" A pair has been found removing pair........");
                                //disard 2 cards
                                Game.add_points_to_player(bonus);
                                removePair(card);
                                players_turn++;
                                //activating computer play....
                                computer_play();
                            } else if (play == 3) {
                                System.out.println("play on 3");
                                reset_Terrain();
                                players_turn++;
                                //activating computer play....
                                computer_play();
                            }
                            LoggerFrame.append_text_to_logger(player1.getPlayer().getName() + " score is : " + Game.get_player_points());
                            System.out.println(player1.getPlayer().getName() + " score is : " + Game.get_player_points());
                            LoggerFrame.append_text_to_logger("***********************************************");
                            System.out.println("***********************************************");

                            break;
                        }
                        case 1: {
                            LoggerFrame.append_text_to_logger("/////////////////////////////////////////////////");

                            System.out.println("/////////////////////////////////////////////////");
                            LoggerFrame.append_text_to_logger("Plyaers " + player2.getPlayer().getName() + " Turn");

                            System.out.println("Plyaers " + player2.getPlayer().getName() + " Turn");
                            LoggerFrame.append_text_to_logger("Round " + player2.getMoves() + " ");
                            System.out.println("Round " + player2.getMoves() + " ");
                            System.out.println("");
                            System.out.println(text);
                            int play = player2.playRound(card);
                            if (play == 2) {
                                show_information_msg("Computer has found a pair ... Be carefull he is good...");
                                LoggerFrame.append_text_to_logger(" A pair has been found removing pair........");
                                Game.add_points_to_computer(bonus);
                                //disard 2 cards
                                removePair(card);
                                players_turn--;
                            } else if (play == 3) {
                                reset_Terrain();
                                players_turn--;
                            }
                            LoggerFrame.append_text_to_logger("Computer score is : " + Game.get_computer_points());
                            System.out.println("Computer score is : " + Game.get_computer_points());

                            LoggerFrame.append_text_to_logger("/////////////////////////////////////////////////");
                            System.out.println("/////////////////////////////////////////////////");
                            break;
                        }
                    }
                    LoggerFrame.append_text_to_logger("");
                }

            }
        };
        //create buttons
        for (int i = 0; i < row * col; i++) {
            buttonGrid[i] = new JButton(this.deck.get_card_at_pos(i).get());
            buttonGrid[i].setEnabled(false);
            buttonGrid[i].setIcon(new ImageIcon(this.deck.get_card_at_pos(i).getCard_image()));
            buttonGrid[i].setDisabledIcon(new ImageIcon(this.deck.get_card_at_pos(i).getCard_image()));

            this.add(buttonGrid[i]);
            buttonGrid[i].addActionListener(listener);
            SwingUtilities.updateComponentTreeUI(this);
        }
        LoggerFrame.append_text_to_logger("Grid has been created...");
        //refresh gui
        this.refresh_Gui();
        this.setTerrainVisible(true);

        //sleep period for terrain to reset
        Thread thread = new Thread() {
            public void run() {
                try {
                    LoggerFrame.append_text_to_logger("Period of picking is acive... You  have 4 seconds to memorize...");
                    Thread.sleep(4 * 1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Terrain_Easy.class.getName()).log(Level.SEVERE, null, ex);
                }
                for (int i = 0; i < row * col; i++) {
                    buttonGrid[i].setEnabled(true);
                }
                reset_Terrain();
            }
        };
        //start the thread
        thread.start();

    }

    private void removePair(Card card) {
        for (int i = 0; i < row * col; i++) {
            if (buttonGrid[i] != null) {
                if (deck.get_card_at_pos(i).get().equalsIgnoreCase(card.get())) {
                    buttonGrid[i].setVisible(false);
                    SwingUtilities.updateComponentTreeUI(this);
                    deleted_items.add(i);
                }
            }
        }
        if (deleted_items.size() == buttonGrid.length) {
            terminateGame();
        }
    }

    private void computer_play() {
        if (deleted_items.size() != buttonGrid.length) {
            show_information_msg("Computer's turn....");
            int value2 = 100000;;
            for (int i = 0; i < 2; i++) {
                int value;
                do {
                    value = rand.nextInt(row * col);
                } while (deleted_items.contains(value) || value == value2);
                value2 = value;
                System.out.println("Button to sellect is : " + value);
                buttonGrid[value].doClick();

            }
            show_information_msg("Your turn....");
        }
    }

    //reset the terrain
    private void reset_Terrain() {
        for (int i = 0; i < row * col; i++) {
            if (buttonGrid[i] != null) {
                buttonGrid[i].setIcon(new ImageIcon(backround));
                SwingUtilities.updateComponentTreeUI(this);
            }
        }
        System.out.println("Terrain has been reformed....");
    }

}
