/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package terrain;

import card.Balloun_Card;
import card.Balls_Card;
import card.Beer_Card;
import card.Bird_Card;
import card.Card;
import card.Char_Card;
import card.Coffee_Card;
import card.Compter_Card;
import card.Cup_Card;
import card.Drink_Card;
import card.Grandma_Card;
import card.Lis_Card;
import card.Tea_Card;
import card.Wizzar_Card;
import card.Table_Class;
import card.Lol_Card;
import card.Plane_Card;
import card.Miky_Card;
import card.Jacket_Card;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author onelove
 */
public class Deck {

    private Card[] deck;

    public void initialize_deck_easy() {
        this.deck = new Card[12];
        try {
            deck[0] = new Beer_Card("Beer_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/1.jpg")), 0);
            deck[1] = new Beer_Card("Beer_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/1.jpg")), 1);
            deck[2] = new Drink_Card("Drink_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/2.jpg")), 2);
            deck[3] = new Drink_Card("Drink_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/2.jpg")), 3);
            deck[4] = new Lis_Card("Lis_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/3.jpg")), 4);
            deck[5] = new Lis_Card("Lis_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/3.jpg")), 5);
            deck[6] = new Tea_Card("Tea_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/4.jpg")), 6);
            deck[7] = new Tea_Card("Tea_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/4.jpg")), 7);
            deck[8] = new Wizzar_Card("Wizzar_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/5.jpg")), 8);
            deck[9] = new Wizzar_Card("Wizzar_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/5.jpg")), 9);
            deck[10] = new Balloun_Card("Balloun_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/6.jpg")), 10);
            deck[11] = new Balloun_Card("Balloun_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/6.jpg")), 11);
        } catch (IOException ex) {
            Logger.getLogger(Deck.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void initialize_deck_medium() {
        this.deck = new Card[20];
        try {
            deck[0] = new Beer_Card("Beer_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/1.jpg")), 0);
            deck[1] = new Beer_Card("Beer_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/1.jpg")), 1);
            deck[2] = new Drink_Card("Drink_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/2.jpg")), 2);
            deck[3] = new Drink_Card("Drink_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/2.jpg")), 3);
            deck[4] = new Lis_Card("Lis_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/3.jpg")), 4);
            deck[5] = new Lis_Card("Lis_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/3.jpg")), 5);
            deck[6] = new Tea_Card("Tea_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/4.jpg")), 6);
            deck[7] = new Tea_Card("Tea_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/4.jpg")), 7);
            deck[8] = new Wizzar_Card("Wizzar_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/5.jpg")), 8);
            deck[9] = new Wizzar_Card("Wizzar_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/5.jpg")), 9);
            deck[10] = new Balloun_Card("Balloun_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/6.jpg")), 10);
            deck[11] = new Balloun_Card("Balloun_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/6.jpg")), 11);
            deck[12] = new Lol_Card("Lol_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/7.jpg")), 12);
            deck[13] = new Lol_Card("Lol_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/7.jpg")), 13);
            deck[14] = new Plane_Card("Plane_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/8.jpg")), 14);
            deck[15] = new Plane_Card("Plane_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/8.jpg")), 15);
            deck[16] = new Miky_Card("Miky_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/9.jpeg")), 16);
            deck[17] = new Miky_Card("Miky_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/9.jpeg")), 17);
            deck[18] = new Jacket_Card("Jacket_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/10.jpeg")), 18);
            deck[19] = new Jacket_Card("Jacket_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/10.jpeg")), 19);
        } catch (IOException ex) {
            Logger.getLogger(Deck.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void initialize_deck_hard() {
        this.deck = new Card[36];
        try {
            deck[0] = new Beer_Card("Beer_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/1.jpg")), 0);
            deck[1] = new Beer_Card("Beer_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/1.jpg")), 1);
            deck[2] = new Drink_Card("Drink_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/2.jpg")), 2);
            deck[3] = new Drink_Card("Drink_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/2.jpg")), 3);
            deck[4] = new Lis_Card("Lis_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/3.jpg")), 4);
            deck[5] = new Lis_Card("Lis_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/3.jpg")), 5);
            deck[6] = new Tea_Card("Tea_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/4.jpg")), 6);
            deck[7] = new Tea_Card("Tea_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/4.jpg")), 7);
            deck[8] = new Wizzar_Card("Wizzar_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/5.jpg")), 8);
            deck[9] = new Wizzar_Card("Wizzar_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/5.jpg")), 9);
            deck[10] = new Balloun_Card("Balloun_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/6.jpg")), 10);
            deck[11] = new Balloun_Card("Balloun_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/6.jpg")), 11);
            deck[12] = new Lol_Card("Lol_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/7.jpg")), 12);
            deck[13] = new Lol_Card("Lol_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/7.jpg")), 13);
            deck[14] = new Plane_Card("Plane_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/8.jpg")), 14);
            deck[15] = new Plane_Card("Plane_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/8.jpg")), 15);
            deck[16] = new Miky_Card("Miky_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/9.jpeg")), 16);
            deck[17] = new Miky_Card("Miky_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/9.jpeg")), 17);
            deck[18] = new Jacket_Card("Jacket_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/10.jpeg")), 18);
            deck[19] = new Jacket_Card("Jacket_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/10.jpeg")), 19);
           
            deck[20] = new Grandma_Card("Grandma_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/11.jpg")), 20);
            deck[21] = new Grandma_Card("Grandma_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/11.jpg")), 21);
            deck[22] = new Coffee_Card("Coffee_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/12.jpeg")), 22);
            deck[23] = new Coffee_Card("Coffee_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/12.jpeg")), 23);
            deck[24] = new Balls_Card("Balls_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/13.jpg")), 24);
            deck[25] = new Balls_Card("Balls_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/13.jpg")), 25);
            deck[26] = new Compter_Card("Compter_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/14.jpg")), 26);
            deck[27] = new Compter_Card("Compter_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/14.jpg")), 27);
            deck[28] = new Cup_Card("Cup_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/15.jpeg")), 28);
            deck[29] = new Cup_Card("Cup_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/15.jpeg")), 29);
            deck[30] = new Bird_Card("Bird_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/16.jpg")), 30);
            deck[31] = new Bird_Card("Bird_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/16.jpg")), 31);
            deck[32] = new Char_Card("Char_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/17.jpg")), 32);
            deck[33] = new Char_Card("Char_Card", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/17.jpg")), 33);
            deck[34] = new Table_Class("Table_Class", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/18.jpg")), 34);
            deck[35] = new Table_Class("Table_Class", ImageIO.read(new File("/home/onelove/Documents/templates/SpartinosGame/src/terrain/images/18.jpg")), 35);
        } catch (IOException ex) {
            Logger.getLogger(Deck.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Card get_card_at_pos(int pos) {
        return deck[pos];
    }

    public int getDeckSize() {
        return this.deck.length;
    }

    public Deck() {

//        initialize_deck_easy();
//        printDeck();
//        shuffleArray();
//        System.out.println("After Sufle");
//        printDeck();
    }

    public void printDeck() {
        System.out.println("");
        System.out.println("new short");
        System.out.println("");
        for (int i = 0; i < deck.length; i++) {
            System.out.println(deck[i].get());
        }
    }

    public static int randInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }

    public void shuffleArray() {
        Random rnd = ThreadLocalRandom.current();
        for (int i = deck.length - 1; i > 0; i--) {
            int index = rnd.nextInt(i + 1);
            // Simple swap
            Card a = deck[index];
            deck[index] = deck[i];
            deck[i] = a;
        }
    }

}
