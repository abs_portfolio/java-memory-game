/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package terrain;

import card.Card;
import card.Pair;
import game_essentials.Game;
import game_essentials.Round;
import game_essentials.ThreadTimer;
import graphics.LoggerFrame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import static terrain.Terrain.timer;

/**
 *
 * @author onelove
 */
public class Terrain_Hard extends Terrain {

    public static final int ttl = 4 * 60 * 1000; // timer limit
    private final static int bonus = 10; // bonus for a succesfull pair 
    private final static int row = 6; //rows of table
    private final static int col = 6; // cols of table
    private JButton[] buttonGrid; // table of buttons itself
    private int players_turn = 0; // define players turn
    private List<Integer> deleted_items; // pairs have been found
    private static Random rand = new Random();
    private List<Pair> pairs;
    private final static int ttl_medium = 3 * 60 * 1000;

    //overwite default constractor to generate the basic attributes
    public Terrain_Hard() throws IOException {
        super(3 * 60 * 1000, 1920, 1080);
        this.setExperimentLayout(new GridLayout(row + 1, col));
        this.setLayout(experimentLayout);
        deleted_items = new ArrayList();
        timer = new ThreadTimer(this);
        pairs = new ArrayList();
    }

    private Card getCardByName(String name) {
        for (int i = 0; i < deck.getDeckSize(); i++) {
            if (deck.get_card_at_pos(i).get().equalsIgnoreCase(name)) {
                return deck.get_card_at_pos(i);
            }
        }
        return null;
    }

    @Override
    public void refresh_Gui() {
        SwingUtilities.updateComponentTreeUI(this);

    }

    @Override
    public void intiializeTerrain(Round player1, Round player2) {
        //initialize timer to 4 minutes
        start_timer();

        //initialize deck
        this.deck.initialize_deck_hard();
        this.deck.shuffleArray();
        this.deck.printDeck();
        this.add(restart_game);
        this.add(new JLabel());
        this.add(new_game);
        this.add(new JLabel());
        this.add(new JButton("dsadaddddd"));
        this.add(new JLabel());

        this.buttonGrid = new JButton[row * col];
        Game.set_pairs((row * col) / 2);
        //create action listener
        ActionListener listener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() instanceof JButton) {
                    log_pair(e);
                    String text = ((JButton) e.getSource()).getText();
                    Card card = getCardByName(text);

                    ((JButton) e.getSource()).setIcon(new ImageIcon(card.getCard_image()));
                    refresh_Gui();
                    switch (players_turn) {
                        case 0: {
                            LoggerFrame.append_text_to_logger("***********************************************");
                            System.out.println("***********************************************");
                            LoggerFrame.append_text_to_logger("Plyaers " + player1.getPlayer().getName() + " Turn");
                            LoggerFrame.append_text_to_logger("Round " + player1.getMoves() + " ");

                            System.out.println("Plyaers " + player1.getPlayer().getName() + " Turn");
                            System.out.println("Round " + player1.getMoves() + " ");
                            System.out.println("");
                            System.out.println(text);

                            int play = player1.playRound(card);
                            if (play == 2) {
                                show_information_msg("You have found a pair GJ!!!!σ");
                                System.out.println("play on 2");
                                LoggerFrame.append_text_to_logger(" A pair has been found removing pair........");
                                //disard 2 cards
                                Game.add_points_to_player(bonus);
                                removePair(card);
                                players_turn++;
                                //activating computer play....
                                computer_play();
                            } else if (play == 3) {
                                //log the unsaccessfull try so computer can be better :)
                                System.out.println("play on 3");
                                //reset the terrain
                                reset_Terrain();
                                //log players turn
                                players_turn++;
                                //activating computer play....
                                computer_play();
                            }
                            LoggerFrame.append_text_to_logger(player1.getPlayer().getName() + " score is : " + Game.get_player_points());
                            System.out.println(player1.getPlayer().getName() + " score is : " + Game.get_player_points());
                            LoggerFrame.append_text_to_logger("***********************************************");
                            System.out.println("***********************************************");

                            break;
                        }
                        case 1: {
                            LoggerFrame.append_text_to_logger("/////////////////////////////////////////////////");

                            System.out.println("/////////////////////////////////////////////////");
                            LoggerFrame.append_text_to_logger("Plyaers " + player2.getPlayer().getName() + " Turn");

                            System.out.println("Plyaers " + player2.getPlayer().getName() + " Turn");
                            LoggerFrame.append_text_to_logger("Round " + player2.getMoves() + " ");
                            System.out.println("Round " + player2.getMoves() + " ");
                            System.out.println("");
                            System.out.println(text);
                            int play = player2.playRound(card);
                            if (play == 2) {
                                show_information_msg("Computer has found a pair ... Be carefull he is good...");
                                LoggerFrame.append_text_to_logger(" A pair has been found removing pair........");
                                Game.add_points_to_computer(bonus);
                                //disard 2 cards
                                removePair(card);
                                players_turn--;
                            } else if (play == 3) {
                                reset_Terrain();
                                players_turn--;
                            }
                            LoggerFrame.append_text_to_logger("Computer score is : " + Game.get_computer_points());
                            System.out.println("Computer score is : " + Game.get_computer_points());

                            LoggerFrame.append_text_to_logger("/////////////////////////////////////////////////");
                            System.out.println("/////////////////////////////////////////////////");
                            break;
                        }
                    }
                    LoggerFrame.append_text_to_logger("");
                }

            }
        };
        //create buttons
        for (int i = 0; i < row * col; i++) {
            buttonGrid[i] = new JButton(this.deck.get_card_at_pos(i).get());
            buttonGrid[i].setEnabled(false);
            buttonGrid[i].setIcon(new ImageIcon(this.deck.get_card_at_pos(i).getCard_image()));
            buttonGrid[i].setDisabledIcon(new ImageIcon(this.deck.get_card_at_pos(i).getCard_image()));

            this.add(buttonGrid[i]);
            buttonGrid[i].addActionListener(listener);
            SwingUtilities.updateComponentTreeUI(this);
        }
        LoggerFrame.append_text_to_logger("Grid has been created...");
        //refresh gui
        this.refresh_Gui();
        this.setTerrainVisible(true);

        //sleep period for terrain to reset
        Thread thread = new Thread() {
            public void run() {
                try {
                    LoggerFrame.append_text_to_logger("Period of picking is acive... You  have 4 seconds to memorize...");
                    Thread.sleep(2 * 1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Terrain_Easy.class.getName()).log(Level.SEVERE, null, ex);
                }
                for (int i = 0; i < row * col; i++) {
                    buttonGrid[i].setEnabled(true);
                }
                reset_Terrain();
            }
        };
        //start the thread
        thread.start();

    }

    private int first_card = Integer.MAX_VALUE;

    private void log_pair(ActionEvent e) {
        for (int i = 0; i < buttonGrid.length; i++) {
            if (buttonGrid[i] != null && buttonGrid[i].isVisible()) {
                if (first_card == Integer.MAX_VALUE && buttonGrid[i] == e.getSource()) {
                    first_card = i;
                    break;
                } else if (first_card != Integer.MAX_VALUE && buttonGrid[i] == e.getSource()) {
                    LoggerFrame.append_text_to_logger("Logging Pair for computer inteligence...");
                    System.out.println("Logging Pair for computer inteligence :: fist card : " + first_card + " , second card os : " + i);
                    pairs.add(new Pair(first_card, i));
                    first_card = Integer.MAX_VALUE;
                    break;
                }

            }
        }
    }

    private void removePair(Card card) {
        for (int i = 0; i < row * col; i++) {
            if (buttonGrid[i] != null) {
                if (buttonGrid[i].isVisible()) {
                    if (deck.get_card_at_pos(i).get().equalsIgnoreCase(card.get())) {
                        buttonGrid[i].setVisible(false);
                        SwingUtilities.updateComponentTreeUI(this);
                        deleted_items.add(i);
                    }
                }
            }
        }
        if (deleted_items.size() == buttonGrid.length) {
            terminateGame();
        }
    }

    private void computer_play() {
        int temp = Integer.MAX_VALUE;
        int value = Integer.MAX_VALUE;
        if (deleted_items.size() != buttonGrid.length) {
            show_information_msg("Computer's turn....");
            int value2 = 100000;;
            for (int i = 0; i < 2; i++) {

                do {
                    value = rand.nextInt(row * col);
                    if (value2 != 100000) {
                        for (int j = 0; j < pairs.size(); j++) {
                            if (pairs.get(j).getFirst_choise() == temp && pairs.get(j).getSecond_choise() == value) {
                                value = value2;
                                break;
                            } else if (pairs.get(j).getFirst_choise() == value && pairs.get(j).getSecond_choise() == temp) {
                                value = value2;
                                break;
                            }
                        }
                    }
                } while (deleted_items.contains(value) || value == value2);
                value2 = value;
                buttonGrid[value].doClick();
                if (i == 0) {
                    temp = value;
                }
            }
            show_information_msg("Your turn....");
        }
    }

    //reset the terrain
    private void reset_Terrain() {
        for (int i = 0; i < row * col; i++) {
            if (buttonGrid[i] != null) {
                buttonGrid[i].setIcon(new ImageIcon(backround));
                buttonGrid[i].setDisabledIcon(new ImageIcon(backround));
                SwingUtilities.updateComponentTreeUI(this);
            }
        }
        System.out.println("Terrain has been reformed....");
    }
}
