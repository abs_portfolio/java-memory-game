/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphics;

import game_essentials.Game;
import game_essentials.Player;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author onelove
 */
public class WellcomeFrame {

    private static JFrame main_frame;
    private static JTextField username;
    private final static String[] difficality_template = {"Easy", "Medium", "Hard"};
    private static JComboBox difficulity_list;
    private static JLabel headerLabel;
    private static JLabel statusLabel;
    private static JPanel controlPanel;
    private static JButton create;
    private static Map<String, Integer> difficulity_map;
    private static Game game;

    static {
        difficulity_map = new HashMap();
        difficulity_map.put(difficality_template[0], new Integer(1));
        difficulity_map.put(difficality_template[1], new Integer(2));
        difficulity_map.put(difficality_template[2], new Integer(3));
    }

    public static void createMainFrame() {
        main_frame = new JFrame("");
        JPanel p = new JPanel();
        //Create the combo box, select item at index 4.
        //Indices start at 0, so 4 specifies the pig.
        difficulity_list = new JComboBox(difficality_template);
        difficulity_list.setSelectedIndex(0);
//        difficulity_list.setBounds(50, 100, 200, 30);
        username = new JTextField();
//        username.setBounds(150, 150, 100, 30);

        p.add(username);
        p.add(difficulity_list);

        main_frame.add(p);
        main_frame.setSize(400, 400);
        main_frame.setVisible(true);
        main_frame.setLayout(new FlowLayout());
        main_frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void showTextFieldDemo() {
        headerLabel.setText("Create a player and test your skils !!");

        JLabel namelabel = new JLabel("User ID: ", JLabel.RIGHT);
        JLabel passwordLabel = new JLabel("Difficality : ", JLabel.CENTER);
        final JTextField userText = new JTextField(6);

        difficulity_list = new JComboBox(difficality_template);
        difficulity_list.setSelectedIndex(0);

        create = new JButton("Create Game");

        create.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String data = "Username " + userText.getText();
                data += ", Difficality: " + difficulity_list.getSelectedItem().toString();
                statusLabel.setText(data);
                //updating logger
                LoggerFrame.append_text_to_logger("Creating game with : ");
                LoggerFrame.append_text_to_logger(data);
                try {
                    //initialize game based on user choise
                    serve_request(userText.getText(), difficulity_map.get(difficulity_list.getSelectedItem().toString()));
                } catch (IOException ex) {
                    Logger.getLogger(WellcomeFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        controlPanel.add(namelabel);
        controlPanel.add(userText);
        controlPanel.add(passwordLabel);
        controlPanel.add(difficulity_list);
        controlPanel.add(create);
        main_frame.setVisible(true);
    }

    public static void prepareGUI() {
        LoggerFrame.append_text_to_logger("Creating Main Frame ..... ");
        main_frame = new JFrame("Welcome to Memory game...");
        main_frame.setSize(400, 400);
        main_frame.setLayout(new GridLayout(3, 1));

        main_frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowEvent) {
                System.exit(0);
            }
        });
        headerLabel = new JLabel("", JLabel.CENTER);
        statusLabel = new JLabel("", JLabel.CENTER);
        statusLabel.setSize(350, 100);

        controlPanel = new JPanel();
        controlPanel.setLayout(new FlowLayout());

        main_frame.add(headerLabel);
        main_frame.add(controlPanel);
        main_frame.add(statusLabel);
        main_frame.setVisible(true);
    }

    public static JFrame getMain_frame() {
        return main_frame;
    }

    public static JTextField getUsername() {
        return username;
    }

    public static void setMain_frame(JFrame main_frame) {
        WellcomeFrame.main_frame = main_frame;
    }

    public static void setUsername(JTextField username) {
        WellcomeFrame.username = username;
    }

    private static void serve_request(String username, Integer choise) throws IOException {
        //hide main frame
        main_frame.setVisible(false);
        //start game
        game = new Game(new Player(username), choise);
    }

    public static void setFrameVisibility(boolean temp) {
        main_frame.setVisible(temp);
    }

    public static Game getGameObj() {
        return WellcomeFrame.game;
    }

}
