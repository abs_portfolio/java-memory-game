/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphics;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.TextArea;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

/**
 *
 * @author onelove
 */
public class LoggerFrame {

    private static JFrame logger;

    private static JPanel log_panel;
    private static JPanel options_panel;
    private static JTextArea text_area;
    private static JButton restart_game;
    private static JButton quit_game;
    private static JButton new_game;
    //history dropdown

    public static void createLoggerFrame() {

        logger = new JFrame("Logger Frame");

        text_area = new JTextArea(20, 40);
        text_area.setFont(new Font("Serif", Font.ITALIC, 13));
        text_area.setEditable(false);
        text_area.setBackground(Color.BLACK);
        text_area.setForeground(Color.GREEN);
        JScrollPane areaScrollPane = new JScrollPane(text_area);
        areaScrollPane.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        areaScrollPane.setSize(10, 10);
        logger.add(areaScrollPane);

        logger.setLayout(new FlowLayout());
        logger.setSize(500, 400);
        logger.setVisible(true);

        logger.setResizable(false);

//        new_game = new JButton("New Game");
//        restart_game = new JButton("Restart Game");
//        quit_game = new JButton("Quit Game");
//
//        options_panel.add(text_area);
//        logger.add(new_game);
//        logger.add(restart_game);
//        logger.add(quit_game);
    }

    private final static String newline = "\n";

    public static void append_text_to_logger(String text) {
        text_area.append("[Logger] :    " + text + newline);

    }

}
