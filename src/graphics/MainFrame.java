/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphics;

import terrain.Deck;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author onelove
 */
public class MainFrame extends JFrame {

    GridLayout experimentLayout = new GridLayout(3, 4);
    private JButton[][] buttonGrid = new JButton[3][4];

    public MainFrame(String temp, Deck deck) {
        super(temp);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        this.setLayout(experimentLayout);
        this.setResizable(false);

        for (int i = 0; i < deck.getDeckSize(); i++) {
            this.add(new JButton(deck.get_card_at_pos(i).get()));
        }

        this.setSize(900, 600);

    }

    public void initializeButtonList(Deck deck,int rows, int cols) {
        for (int i = 0; i < 10; i++) {
            
        }
    }

    public void actionPerformed(ActionEvent e) {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 5; col++) {
                if (buttonGrid[row][col] == e.getSource()) {
                    System.out.println(buttonGrid[row][col].getName());
                }
            }
        }
    }

}
