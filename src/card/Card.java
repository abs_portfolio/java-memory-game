package card;

import java.awt.Image;
import java.awt.image.BufferedImage;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author onelove
 */
public abstract class Card {

    protected String card_name;
    protected BufferedImage card_image;
    protected int id;

    public abstract String get();

    public Card() {
    }

    public Card(String card, BufferedImage img) {
        this.card_image = img;
        this.card_name = card;

    }

    public Card(String card, int  num) {
       
        this.card_name = card;
        this.id = num;

    }
    public Card(String card, BufferedImage img, int id) {
        this.card_image = img;
        this.card_name = card;
        this.id = id;

    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getCard_name() {
        return card_name;
    }

    public BufferedImage getCard_image() {
        return card_image;
    }

    
    public void setCard_name(String card_name) {
        this.card_name = card_name;
    }

    public void setCard_image(BufferedImage card_image) {
        this.card_image = card_image;
    }
    
    

}
