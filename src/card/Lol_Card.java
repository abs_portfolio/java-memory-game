/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package card;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author onelove
 */
public class Lol_Card extends Card {


    public Lol_Card(String card, BufferedImage img) {
        super(card, img);
    }

    public Lol_Card(String card, BufferedImage img, int id) {
        super(card, img, id);
    }

    public Lol_Card(String card, int num) {
        super(card, num);
    }

    @Override
    public String get() {
        return "Lol_Card";
    }
}
