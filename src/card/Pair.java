/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package card;

/**
 *
 * @author onelove
 */
public class Pair {
    int first_choise;
    int second_choise;

    public int getFirst_choise() {
        return first_choise;
    }

    public int getSecond_choise() {
        return second_choise;
    }

    public void setFirst_choise(int first_choise) {
        this.first_choise = first_choise;
    }

    public void setSecond_choise(int second_choise) {
        this.second_choise = second_choise;
    }

    public Pair(int first_choise, int second_choise) {
        this.first_choise = first_choise;
        this.second_choise = second_choise;
    }
    
}
