/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package card;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author onelove
 */
public class Balloun_Card extends Card {



    public Balloun_Card(String card, BufferedImage img) {
        super(card, img);
    }

    public Balloun_Card(String card, int num) {
        super(card, num);
    }

    public Balloun_Card(String card, BufferedImage img, int id) {
        super(card, img, id);
    }

    @Override
    public String get() {
        return "Balloun_Card";
    }
}
