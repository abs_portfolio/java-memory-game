/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package card;

import java.awt.image.BufferedImage;


/**
 *
 * @author onelove
 */
public class Char_Card extends Card {

    public Char_Card(String card, BufferedImage img) {
        super(card, img);
    }

    public Char_Card(String card, BufferedImage img, int id) {
        super(card, img, id);
    }

    public Char_Card(String card, int num) {
        super(card, num);
    }

    @Override
    public String get() {
        return "Char_Card";
    }
}
