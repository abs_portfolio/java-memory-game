/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connector;

/**
 *
 * @author onelove
 */
public class History {
    private int record_id;
    private String player_1;
    private String player_2;
    private int points_1;
    private int points_2;

    public History(int record_id, String player_1, String player_2, int points_1, int points_2) {
        this.record_id = record_id;
        this.player_1 = player_1;
        this.player_2 = player_2;
        this.points_1 = points_1;
        this.points_2 = points_2;
    }
    public History( String player_1, String player_2, int points_1, int points_2) {
        this.record_id = record_id;
        this.player_1 = player_1;
        this.player_2 = player_2;
        this.points_1 = points_1;
        this.points_2 = points_2;
    }

    
    
    public int getRecord_id() {
        return record_id;
    }

    public String getPlayer_1() {
        return player_1;
    }

    public String getPlayer_2() {
        return player_2;
    }

    public int getPoints_1() {
        return points_1;
    }

    public int getPoints_2() {
        return points_2;
    }

    public void setRecord_id(int record_id) {
        this.record_id = record_id;
    }

    public void setPlayer_1(String player_1) {
        this.player_1 = player_1;
    }

    public void setPlayer_2(String player_2) {
        this.player_2 = player_2;
    }

    public void setPoints_1(int points_1) {
        this.points_1 = points_1;
    }

    public void setPoints_2(int points_2) {
        this.points_2 = points_2;
    }
    
    
    
}
