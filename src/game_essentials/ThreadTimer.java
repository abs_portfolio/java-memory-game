/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game_essentials;

import graphics.LoggerFrame;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import terrain.Terrain;
import terrain.Terrain_Easy;

/**
 *
 * @author onelove
 */
public class ThreadTimer extends Thread {

    Terrain ter;

    public ThreadTimer(Terrain t) {
        this.ter = t;
    }

    @Override
    public void run() {
        System.out.println("Timer has started...");
        LoggerFrame.append_text_to_logger("Timer Has Started.... time to live is set to : " + ter.ttl);
        try {
            ThreadTimer.sleep(ter.ttl);
            ter.terminateGame(true);
        } catch (InterruptedException ex) {
            LoggerFrame.append_text_to_logger("Thread was interapted so probably the game has come to an end...");

        }

    }

}
