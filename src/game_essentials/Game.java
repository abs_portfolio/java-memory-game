/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game_essentials;

import connector.History;
import graphics.LoggerFrame;
import terrain.Deck;
import graphics.MainFrame;
import graphics.WellcomeFrame;
import java.io.IOException;
import java.util.List;
import terrain.Terrain;
import terrain.Terrain_Easy;
import terrain.Terrain_Hard;
import terrain.Terrain_Medium;

/**
 *
 * @author onelove
 */
public class Game {

    Round rounds[] = new Round[2];
    Terrain terain;
    private static Player computer = new Player("Computer", "comp");
    private static Player human;
    private static int player_score = 0;
    private static int computer_score = 0;
    private static int pairs_left;

    public Game(Player player, int difficality) throws IOException {
        human = player;
        Round computer_round = new Round(computer);
        Round player_roun = new Round(player);
        switch (difficality) {
            case 1: {
                terain = new Terrain_Easy();
                terain.intiializeTerrain(player_roun, computer_round);
                break;
            }
            case 2: {
                terain = new Terrain_Medium();
                terain.intiializeTerrain(player_roun, computer_round);
                break;
            }
            case 3: {
                terain = new Terrain_Hard();
                terain.intiializeTerrain(player_roun, computer_round);
                break;
            }
        }
    }

    public void new_game() {
        LoggerFrame.append_text_to_logger("");
        LoggerFrame.append_text_to_logger("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
        LoggerFrame.append_text_to_logger("--------------New Game-----------------------");
        zero_points();
        WellcomeFrame.setFrameVisibility(true);
        this.terain.dispose();
        this.terain = null;
    }

    public void rest_game() throws IOException {
        //zero points
        zero_points();
        //recreate the terrain
        this.terain.dispose();
        //recreate the rounds.
        Round computer_round = new Round(computer);
        Round player_roun = new Round(human);

        if (this.terain instanceof Terrain_Easy) {
            terain = null;
            terain = new Terrain_Easy();
            terain.intiializeTerrain(player_roun, computer_round);
        } else if (this.terain instanceof Terrain_Medium) {
            terain = null;
            terain = new Terrain_Medium();
            terain.intiializeTerrain(player_roun, computer_round);
        } else if (this.terain instanceof Terrain_Hard) {
            terain = null;
            terain = new Terrain_Hard();
            terain.intiializeTerrain(player_roun, computer_round);
        }
    }

    public static void add_points_to_player(int points) {
        player_score = player_score + points;

    }

    public static void add_points_to_computer(int points) {
        computer_score = computer_score + points;

    }

    public static void zero_points() {
        player_score = 0;
        computer_score = 0;
    }

    public static int get_player_points() {
        return player_score;
    }

    public static int get_computer_points() {
        return computer_score;
    }

    public static void set_pairs(int temp) {
        pairs_left = temp;
    }

    public static int get_pairs_left() {
        return pairs_left;
    }

    public static void reduce_pairs_left() {
        pairs_left--;
    }

    public static String culculateWinner() {
        if (computer_score > player_score) {
            return "Winner Player :: Computer";
        } else if (computer_score < player_score) {
            return "Winner Player :: " + human.getName();
        } else {
            return "We have a Tie!";
        }
    }

    public static History historyResult() {
        return new History(human.getName(), computer.getName(), player_score, computer_score);
    }

}
