/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game_essentials;

import card.Card;

/**
 *
 * @author onelove
 */
public class Round {

    private int round_limit = 2;
    private int moves = 0;
    private Player player;
    private Card temp;

    public Round(Player player) {
        moves = 0;
        this.player = player;
    }

    public boolean validateCards(Card card1, Card card2) {
        if (moves < round_limit) {
            if (card1.get().equalsIgnoreCase(card2.get())) {
                player.addScore(2);
                return true;
            }
        }
        return false;
    }

    public int playRound(Card card) {
        if (moves == 0) {
            temp = card;
            moves++;
            return 1;
        } else if (moves == 1) {
            if (validateCards(card, temp)) {
                moves = 0;
                temp = null;
                return 2;
            }
        }
        moves = 0;
        return 3;
    }

    public int getRound_limit() {
        return round_limit;
    }

    public int getMoves() {
        return moves;
    }

    public Player getPlayer() {
        return player;
    }

}
