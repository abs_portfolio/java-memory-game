package game_essentials;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author onelove
 */
public class Player {

    private String name;
    private String surname;
    int score;

    public Player(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }
    public Player(String name) {
        this.name = name;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setScore(int score) {
        this.score = score;
    }

    
    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getScore() {
        return score;
    }
    
    public void addScore(int score){
        this.score = score + this.score; 
    }
    
    
}
